(add-to-list 'load-path "./elisp")

(require 'hamlib)

(hamlib-get-frequency)

(hamlib-list-modes)

(hamlib-get-mode)

;;;;
;; Memories
;;;;

;; 40m
(progn
  (hamlib-set-frequency 7175000)
  (hamlib-set-mode "LSB" -1))

;; mid 40m
(progn
  (hamlib-set-frequency 7237000)
  (hamlib-set-mode "LSB" -1))


;; 20m
(progn
  (hamlib-set-frequency 14225000)
  (hamlib-set-mode "USB" -1))

;; mid 20m
(progn
  (hamlib-set-frequency 14265000)
  (hamlib-set-mode "USB" -1))

;; 10m
(progn
  (hamlib-set-frequency 28300000)
  (hamlib-set-mode "USB" -1))

;; mid 10m
(progn
  (hamlib-set-frequency 28400000)
  (hamlib-set-mode "USB" -1))

;; 2m
(progn
  (hamlib-set-frequency 144000000)
  (hamlib-set-mode "FM" -1))

;; mid 2m
(progn
  (hamlib-set-frequency 146000000)
  (hamlib-set-mode "FM" -1))

;; 70cm
(progn
  (hamlib-set-frequency 420000000)
  (hamlib-set-mode "FM" -1))

;; mid 70cm
(progn
  (hamlib-set-frequency 435000000)
  (hamlib-set-mode "FM" -1))

;; turn the VFO
(hamlib-shift-frequency -3000)

(hamlib-shift-frequency -1000)

(hamlib-shift-frequency 1000)

(hamlib-shift-frequency 3000)
