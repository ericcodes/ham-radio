// This generates a cover to fit over the top of a battery to protect
// its contacts from being shorted.
//
// There is a slot in the side of the box that allows for a two port
// powerpole connector to slide into place

// The thickness of the walls. Note: A double powerpole groves into a 2mm wall
wallThickness = 2;

// Size of the battery from left to right
width = 150;

// The size of the battery from front to back
depth = 65;

// How tall do you want the walls to be
height = 32;

// The width of the powerpole connector
powerpoleWidth = 14;

// Render a test print model
proof = false;

module innerRect() {
    translate([wallThickness, wallThickness, 0])
    square([width, depth]);
}

module outerRect() {
    square([width+wallThickness*2,depth+wallThickness*2]);
}

module walls() {
    translate([0,0,wallThickness])
    difference() {
    linear_extrude(height)
    difference() {
        outerRect();
        innerRect();
        
    }
    powerpoleCutout();
    }
}

module lid() {
    linear_extrude(wallThickness)
    outerRect();
}

module powerpoleCutout() {
    color("red")
    translate([0, wallThickness+depth/2-powerpoleWidth/2, 0])
    linear_extrude(height)
    square([wallThickness, powerpoleWidth]);
}

module cover() {
    walls();
    lid();
}

if(!proof) {
    cover();
} else {
    difference() {
        cover();
        translate([wallThickness+10, wallThickness+10, 0])
            linear_extrude(height+wallThickness)
            outerRect();
    }
}

