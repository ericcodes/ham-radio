;;; fle-mode.el --- An FLE Emacs mode for keyboard warriors -*- lexical-binding:t -*-

;; Copyright (C) 2024 Eric Moritz

;; Author: Eric Moritz <eric@ericcodes.io>
;; Version: 0.1.0-pre
;; URL: https://gitlab.com/ericcodes/ham-radio/elisp
;; Package-Requires: ((emacs "25.2"))
;; Keyword: fle, comm, languages

;; This program is not part of GNU Emacs
;;
;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program; if not, write to the Free Software Foundation, Inc.,
;; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
;;

;;; Commentary:

;; This package provides a major mode for Fast Log Entry (FLE) files.
;; It provides both CAT control via hamlib. It integrates with qrz.com
;; for callsign lookups.

;; See https://df3cb.com/fle/documentation/ for FLE Format
;;
;; Some code was adapted from https://github.com/sfromm/fle-mode
;;
;; Emacs Lisp pointers:
;; - https://www.gnu.org/software/emacs/manual/html_node/elisp/Rx-Constructs.html
;; - https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html

;;; Code:

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.fle.txt\\'" . fle-mode))

;;; Constants ======================================================
(defconst fle-mode-version "0.1.0-pre")

;;; Customizable Variables =========================================

(defgroup fle-mode nil
  "Major mode for Fast Log Entry (FLE) log files"
  :group 'languages)

(defcustom fle-rigctl-command "rigctl -m 2 -r localhost:4532"
  "The base rigctl command to invoke. Defaults to rigctld"
  :type 'string
  :group 'fle-mode)

(defcustom fle-mode-hook nil
  "*Hook called by `fle-mode`."
  :type 'hook
  :group 'fle-mode)

(defvar fle-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "C-c C-c") 'fle-73)
    (define-key map (kbd "C-c C-e") 'fle-toggle-ptt)
    (define-key map (kbd "C-c C-q") 'fle-qrz)
    (define-key map (kbd "C-c l q") 'fle-lookup)
    (define-key map (kbd "C-c l p") 'fle-lookup-pota)
    (define-key map (kbd "C-c C-f") 'fle-get-freq)
    (define-key map (kbd "C-c M-f") 'fle-set-freq)
    (define-key map (kbd "M-<left>") 'fle-shift-down-100)
    (define-key map (kbd "M-<down>") 'fle-shift-down-1k)
    (define-key map (kbd "M-S-<down>") 'fle-shift-down-3k)
    (define-key map (kbd "M-<right>") 'fle-shift-up-100)
    (define-key map (kbd "M-<up>") 'fle-shift-up-1k)
    (define-key map (kbd "M-S-<up>") 'fle-shift-up-3k)
    map))

;;; Syntax Highlighting =============================================
;; Syntax highlighting provided thanks to https://github.com/sfromm/fle-mode
(defvar fle-mode-syntax-table
  (let ((st (make-syntax-table)))
    ;; (modify-syntax-entry ?# "<" st)  ;; All #'s start comments.
    (modify-syntax-entry ?\n ">" st) ;; All newlines end comments.
    (modify-syntax-entry ?\r ">" st) ;; All linefeeds end comments.
    st)
  "Syntax table for Arista `fle-mode'.")

(defconst fle-supported-bands
  '("2190m" "630m" "560m" "160m" "80m" "60m" "40m" "30m" "20m" "17m" "15m"
    "12m" "10m" "6m" "4m" "2m" "1.25m" "70cm" "33cm" "23cm" "13cm" "9cm"
    "6cm" "3cm" "1.25cm" "6mm" "4mm" "25mm" "2mm" "1mm")
  "Supported bands in FLE.")

(defconst fle-supported-modes
  '("CW" "SSB" "AM" "FM" "RTTY" "FT8" "PSK" "JT65" "JT9" "FT4" "JS8" "ARDOP"
    "ATV" "C4FM" "CHIP" "CLO" "CONTESTI" "DIGITALVOICE" "DOMINO" "DSTAR" "FAX"
    "FSK441" "HELL" "ISCAT" "JT4" "JT6M" "JT44" "MFSK" "MSK144" "MT63" "OLIVIA"
    "OPERA" "PAC" "PAX" "PKT" "PSK2K" "Q15" "QRA64" "ROS" "RTTYM" "SSTV" "T10"
    "THOR" "THRB" "TOR" "V4" "VOI" "WINMOR" "WSPR")
  "Supported modes in FLE.")

(defconst fle-date-regex
  (rx bol (0+ space) "date" space (repeat 4 digit) "-" (repeat 2 digit) "-" (repeat 2 digit))
  "Regular expression for FLE date format.")

(defconst fle-hour-min-regex
  (rx (or bol space) (or (repeat 4 digit) (repeat 2 digit)) space)
  "Regular expression for FLE hour and minute format.")

;; A collection of regexs to match callsigns
;;   https://regex101.com/library/6QhGuD
;;   https://regex101.com/library/uP6xD2
(defconst fle-callsign-regex
  (rx
   (0+ alnum "/")
   (repeat 1 3 alpha) (1+ digit) (repeat 0 3 alpha)
   (0+ "/" alnum))
  "Regular expression for a callsign.")

(defconst fle-operator-regex
  (rx "@" (1+ alnum))
  "Regular expression for operator name.")

(defconst fle-frequency-regex
  (rx (repeat 1 3 digit) "." (repeat 3 digit))
  "Regular expression for operating frequency.")

;; example #JN49
(defconst fle-gridlocator-regex
  (rx (zero-or-one "#")  (repeat 2 alpha) (repeat 2 digit) (repeat 0 2 alnum))
  "Regular expression for grid locator.")

;; example w6/ct-226
(defconst fle-sota-regex
  (rx (repeat 1 3 alnum) "/" (repeat 2 alnum) "-" (repeat 3 digit))
  "Regular expression for SOTA summit ID.")

;; example DA-1234
(defconst fle-pota-regex
  (rx (repeat 1 3 alnum) "-" (repeat 4 5 digit))
  "Regular expression for POTA park ID.")

(defconst fle-qso-remark-regex
  (rx "<" (0+ space) (1+ print) (0+ space) ">")
  "Regular expression for QSO remark syntax.")

(defconst fle-qso-comment-regex
  (rx "{" (0+ space) (1+ print) (0+ space) "}")
  "Regular expression for QSO comment syntax.")

(defconst fle-comment-regex
  (rx bol "#" (0+ print))
  "Regular expression for syntax commenting a line.")

(defconst fle-supported-bands-regex
  (regexp-opt fle-supported-bands 'words)
  "Regular expressions for supported bands in FLE.")

(defconst fle-supported-modes-regex
  (regexp-opt fle-supported-modes 'words)
  "Regular expressions for supported modes in FLE.")

(defconst fle-supported-headers-regex
  (regexp-opt
   '("mycall" "mygrid" "operator" "qslmsg" "syn" "keyword"
     "fle_header" "mywwff" "mysota" "mypota" "nickname") 'words)
  "Regular expressions for FLE headers.")

;; For font-lock faces, see
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html
(defvar fle-font-lock-keywords
  (list
   (list fle-comment-regex 0 font-lock-comment-face)
   (list fle-qso-remark-regex 0 font-lock-string-face)
   (list fle-qso-comment-regex 0 font-lock-comment-face)
   (list fle-date-regex 0 font-lock-string-face)
   (list fle-hour-min-regex 0 font-lock-string-face)
   (list fle-frequency-regex 0 font-lock-builtin-face)
   (list fle-supported-bands-regex 0 font-lock-type-face)
   (list fle-supported-modes-regex 0 font-lock-type-face)
   (list fle-supported-headers-regex 0 font-lock-constant-face)
   (list fle-gridlocator-regex 0 font-lock-string-face)
   (list fle-callsign-regex 0 font-lock-keyword-face)
   (list fle-operator-regex 0 font-lock-string-face)
   (list fle-sota-regex 0 font-lock-builtin-face)
   (list fle-pota-regex 0 font-lock-builtin-face)
   )
  "Font locking definitions for FLE mode.")


;;; Commands ========================================================
(defun fle-shift-up-100 ()
  "Shift up 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency 100))

(defun fle-shift-up-1k ()
  "Shift up 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency 1000))

(defun fle-shift-up-3k ()
  "Shift up 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency 3000))

(defun fle-shift-down-100 ()
  "Shift down 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency -100))

(defun fle-shift-down-1k ()
  "Shift down 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency -1000))

(defun fle-shift-down-3k ()
  "Shift down 1 kHz"
  (interactive)
  (fle-rigctl-shift-frequency -3000))

(defun fle-73 ()
  "End the QSO under point by appending the date and time"
  (interactive)
  (let* ((now (current-time))
         (date (fle--format-date now))
         (time (fle--format-time now)))
    (save-excursion
      (move-beginning-of-line nil)
      (insert (format "%s %s " date time)))))

(defun fle-set-freq ()
  "Set the rig's frequency to the QSO under point's frequency"
  (interactive)
  (save-excursion
    ;; move to the first instance of a callsign
    (end-of-line)
    (search-backward-regexp fle-frequency-regex)
  (let* ((mhz (thing-at-point 'number t)))
    (if mhz
        (let* ((hz (floor (* (expt 10 6) mhz))))
          (fle-rigctl-set-frequency hz))
        (error "not a frequency")))))

(defun fle-get-freq ()
  "Insert the rig's current frequency at point"
  (interactive)
    (insert (fle--format-freq (fle-rigctl-get-frequency)))
    (insert " "))

(defun fle-toggle-ptt ()
  "Toggle the PTT between tx and rx-mic. Allows for hands on the keyboard keying"
  (interactive)
  (let ((ptt (fle-rigctl-get-ptt)))
        (pcase ptt
          ('rx (progn (fle-rigctl-set-ptt 'tx-mic)
                      (message "Transmitting...")))
          (_ (progn (fle-rigctl-set-ptt 'rx)
                    (message "Receiving..."))))))

(defun fle-qrz ()
  "open the qrz.com page for QSO under point's callsign"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (search-forward-regexp fle-callsign-regex)
    (let* ((callsign (thing-at-point 'word t)))
      (browse-url (format "https://www.qrz.com/db/%s" callsign)))))

(defun fle-lookup ()
  "Log the name and grid locator for the QSO under point using qrz.com's API"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (search-forward-regexp fle-callsign-regex)
    (let* ((buf (current-buffer))
           (callsign (thing-at-point 'word t)))
      (when callsign
        ;; insert the name and grid
        (setq name-and-grid (fle--qrz-lookup-callsign callsign))
        (with-current-buffer buf
          (end-of-line)
          (fle--insert-name-and-grid name-and-grid))))))

(defun fle-lookup-pota ()
  "lookup the POTA data for the reference on the current line"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (re-search-forward fle-pota-regex)
    (setq park-ref (thing-at-point 'symbol))
    (setq park (fle--pota-lookup-park park-ref))
    (setq grid (alist-get 'grid6 park))
    (setq grid (if grid grid
                 (alist-get 'grid4 park)))
    (setq name (alist-get 'name park))
    (setq parktype (alist-get 'parktypeDesc park))

    (when name
      (end-of-line)
      (insert " < " park-ref " " name)
      (when parktype
        (insert " " parktype))
      (insert " >"))

    (when grid
      (end-of-line)
      (insert " #" grid))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rigctl library functions
(defun fle-rigctl (command)
  (shell-command-to-string (format "%s %s" fle-rigctl-command command)))

(defun fle-rigctl-get-ptt ()
  (let ((ptt-number (string-to-number (fle-rigctl "get_ptt"))))
    (fle-rigctl--ptt-to-atom ptt-number)))

(defun fle-rigctl-set-ptt (ptt)
  (let ((ptt-number (fle-rigctl--atom-to-ptt ptt)))
    (fle-rigctl (format "set_ptt %s" ptt-number))))

(when nil
  (fle-rigctl-get-ptt)
  (fle-rigctl-set-ptt 'tx-mic)
  (fle-rigctl-set-ptt 'rx))

(defun fle-rigctl--ptt-to-atom (ptt-number)
  (pcase ptt-number
    (0 'rx)
    (1 'tx)
    (2 'tx-mic)
    (3 'tx-data)))

(defun fle-rigctl--atom-to-ptt (ptt)
  (pcase ptt
    ('rx 0)
    ('tx 1 )
    ('tx-mic 2)
    ('tx-data 3)))

(defun fle-rigctl-list-modes ()
  (fle-rigctl "'M' '?'"))

(defun fle-rigctl-get-mode ()
  (fle-rigctl "m"))

(defun fle-rigctl-set-mode (mode passband)
  (fle-rigctl (format "M %s %d" mode passband)))

(defun fle-rigctl-set-frequency (hz)
  (string-to-number (fle-rigctl (format "F %d" hz))))

(defun fle-rigctl-get-frequency ()
  (string-to-number (fle-rigctl "f")))

(defun fle-rigctl-shift-frequency (hz)
  (let* ((current (fle-rigctl-get-frequency))
         (new (+ current hz)))
    (message (fle--format-freq-long new))
    (fle-rigctl-set-frequency new)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FLE library functions
(defun fle--format-freq (hz)
  (let* ((mhz (* hz (expt 10 -6))))
    (format "%.3f" mhz)))

(defun fle--format-freq-long (hz)
  (let* ((mhz (* hz (expt 10 -6))))
    (format "%f" mhz)))

(defun fle--format-fle-rigctl-mode (mode)
  (pcase (intern mode)
    ('AM "am")
    ('CW "cw")
    ((or 'USB 'LSB) "ssb")
    ('RTTY "rtty")
    (_ "???")))

(defun fle--format-time (time)
  (format-time-string "%H%M" time "UTC0"))

(defun fle--format-date (time)
  (format "%s" (format-time-string "%Y-%m-%d" time "UTC0")))

;; Convert the output of `rigctl f m` to a formatted (frequency mode) pair
(defun fle--freq-and-mode (lines)
  (let* ((lines (split-string lines))
         (freq (fle--format-freq (string-to-number (first lines))))
         (mode (fle--format-fle-rigctl-mode (second lines))))
    `(,freq ,mode)))

(defun fle--insert-name-and-grid (name-and-grid)
  (let* ((name (alist-get 'name name-and-grid))
         (grid (alist-get 'grid name-and-grid)))

    (when (and name grid)
      (insert " "))

    (when name
      (insert (format "@%s" name)))

    (when (and name grid)
      (insert " "))

    (when grid
      (insert (format "#%s" grid)))))

(defun fle--insert-park (park)
  (let* ((grid (alist-get 'grid6 park)))
    (when grid
      (insert (format "#%s" grid)))))

(defun fle--pota-lookup-park (park-id)
  (setq resp (url-retrieve-synchronously (format "https://api.pota.app/park/%s" park-id)))
  (setq data (with-current-buffer resp
               (goto-char (point-min))
               ;; find the start of the response body
               (re-search-forward "^$" nil 'move) ; skip headers
               (json-parse-buffer :object-type 'alist :null-object nil :false-object nil)))
  data)

(defun fle--qrz-lookup-callsign (callsign)
  (setq session-id (fle--qrz-get-session-id))
  (setq form `(("s" ,session-id) ("callsign" ,callsign)))
  (setq dom (fle--qrz-xmldata-retrieve form))
  (setq callsign-tag (dom-child-by-tag dom 'Callsign))
  (setq nickname-tag (dom-child-by-tag callsign-tag 'nickname))
  (setq fname-tag (dom-child-by-tag callsign-tag 'fname))
  (setq grid-tag (dom-child-by-tag callsign-tag 'grid))
  (setq fname (dom-text fname-tag))
  (setq nickname (dom-text nickname-tag))
  (setq name (first (split-string (or (unless (string-empty-p nickname) nickname)
                                      (unless (string-empty-p fname) fname)))))
  (setq grid (dom-text grid-tag))
  `((name . ,name)
    (grid . ,grid)))

(defun fle--qrz-xmldata-retrieve (form)
  (setq form (url-build-query-string form))

  (setq resp (let ((url-request-method "POST")
                   (url-request-data form)
                   (url-request-extra-headers `(("Content-Type" . "application/x-www-form-urlencoded"))))
               (setq resp (url-retrieve-synchronously "https://xmldata.qrz.com/xml/current/"))))

  (with-current-buffer resp
    ;; jump to the start of the buffer
    (goto-char (point-min))

    ;; find the start of the response body
    (re-search-forward "^$" nil 'move) ; skip headers

    (setq dom (xml-parse-region))
    (setq session-tag (dom-child-by-tag dom 'Session))
    (setq error-tag (dom-child-by-tag session-tag 'Error))
    (if error-tag
        (error "QRZ Error: %s" (dom-text error-tag))
      dom)))

(defun fle--qrz-get-session-id ()
  (setq authinfo (auth-source-search :max 1
                            :host "xmldata.qrz.com"
                            :require '(:user :secret)
                            :create t))
  (unless authinfo
    (error "please configure an auth-source for xmldata.qrz.com See: https://systemcrafters.net/emacs-tips/using-encrypted-passwords/"))

  (setq authinfo (car authinfo))
  (setq user (plist-get authinfo :user))
  (setq pass (funcall (plist-get authinfo :secret)))

  ;; TODO cache the session-id if needed
  (setq dom (fle--qrz-xmldata-retrieve `(("username" ,user)
                                         ("password" ,pass)
                                         ("agent" "emacs-fle-mode"))))

  (setq session-tag (dom-child-by-tag dom 'Session))
  (setq key-tag (dom-child-by-tag session-tag 'Key))
  (setq session-id (dom-text key-tag))
  session-id)

(define-derived-mode fle-mode text-mode "FLE"
  "Major mode for Fast Log Entry (FLE) keyboard warriors"
  :group 'fle-mode
  (kill-all-local-variables)
  (use-local-map fle-mode-map)
  (set-syntax-table fle-mode-syntax-table)
  (setq major-mode 'fle-mode
        mode-name "abc"
        comment-start "#"
        comment-end ""
        comment-start-skip "# +")
  (set (make-local-variable 'font-lock-defaults) '(fle-font-lock-keywords))
  (run-hooks 'fle-mode-hook))

(provide 'fle-mode)
