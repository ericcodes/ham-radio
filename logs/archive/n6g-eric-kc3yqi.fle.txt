# Header
mycall N6G
mygrid FM19je
operator KC3YQI


2024-06-09 0306

20m
14.330 ssb

# CQ CQ CQ This is November Six Golf special event station for Pride Month, QRZ?
2024-06-09 0316 ki5zsa 59 58
2024-06-09 0319 kc0cqd 55 56

14.307 ssb

2024-06-09 0338 hi3sv 55 59
2024-06-09 0340 tk5ae 57 59
2024-06-09 0343 oz6cm 47 55 < 0600 in Denmark >
2024-06-09 0345 i3zf 45 55 < Tough to hear; likely wrong callsign :( >

####
# 10min Script:
####

# This is November Six Golf, a special event station for CQ-Pride,
# celebrating Pride Month on the air.  For more information about the
# event, see our page on QRZ or go to PrideRadio.group

####
# Short Script:
####
# CQ CQ CQ This is November Six Golf specia event station for Pride Month, QRZ?

####
# Ask if the frequency is free:
####

# This is November Six Golf
# Is this frequency in use?
# Is this frequency in use?
# Is this frequency in use?

# NOTE: Remember to call KC3YQI every hour

40m 7.175 SSB
7.213
# Note: 7.218 interferes with Atles' gear
7.218 SSB
2024-06-15 2303
2024-06-15 2313 kq4en 59 59 @Reid < NC >
2024-06-15 2317 kr1st 59 57
2024-06-15 2320 k1ajt 54 55
2024-06-15 2322 w1plm 55 57 @Paul

70cm DIGITALVOICE
2024-06-16 0226 w5dx 59 59 @Sierra #FN31pr
2024-06-16 0226 w1aw 59 59 #FN31pr < ARRL HQ Station >
2024-06-16 0227 nj1q 59 59 @Joe #FN41hp
