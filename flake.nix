{
  # 1. Defined a "systems" inputs that maps to only ["x86_64-linux"]
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    systems.url = "github:nix-systems/x86_64-linux";
    utils.url = "github:numtide/flake-utils";
    utils.inputs.systems.follows = "systems";

    flecli = {
      url = "github:ericmoritz/FLEcli/support-pota-hunters";
      flake = false;
    };

    potacat = {
      url = "gitlab:ericcodes/potacat";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    mrl = {
      url = "git+https://git.sr.ht/~codelongandpros/mrl";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    cloudlogbashcat = {
      url = "git+https://github.com/g0wfv/CloudlogBashCat.git";
      flake = false;
    };

    hamdashboard-src = {
      url = "github:VA3HDL/hamdashboard";
      flake = false;
    };

    nixpkgs-radio.url = "github:lasandell/nixpkgs-radio";
  };

  outputs = { self, nixpkgs, utils, flecli, mrl, potacat, cloudlogbashcat, hamdashboard-src, nixpkgs-radio, ... }: (utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      mrlPkg = mrl.packages.${system}.mrl;
      potacatPkg = potacat.defaultPackage.${system};
    in
    rec {
      packages = {
        FLEcli = pkgs.buildGoModule {
          name = "FLEcli";
          version = "0.1.7";
          vendorHash = "sha256-LYdLRCBVw9fucDqK56LWatIVO69wGcU6wmarbLE4Tls=";
          src = flecli;
        };
        cloudlogbashcat = pkgs.writeScriptBin "cloudlogbashcat"
          ''
${pkgs.bash}/bin/bash ${cloudlogbashcat}/cloudlogbashcat.sh
'';
        hamdashboard = pkgs.runCommand "hamdashboard" {}
          ''
mkdir -p $out/bin
mkdir -p $out/html
cp -a ${hamdashboard-src}/* $out/html/

# configure the dashboard
cd $out/html
sed -i 's/VA3HDL/K3FNB/g' config.js
sed -i 's/FN04ga/FM19je/g' config.js
sed -i 's/pi-star.local/pi-star/g' config.js
sed -i 's/43.39961001/39.186667/g' config.js
sed -i 's/44.0157/39.186667/g' config.js
sed -i 's/-78.53212031/-77.21/g' config.js
sed -i 's/-79.4591/-77.21/g' config.js
sed -i 's/-78.79,44.09/-77.21,39.18/g' config.js
sed -i 's/KNQA_loop.gif/KLWX_loop.gif/g' config.js

# Create the start script
cat << EOF > $out/bin/hamdashboard
${pkgs.firefox}/bin/firefox --new-window $out/html/hamdash.html
echo "hamdashboard started"
sleep infinity
EOF
chmod +x $out/bin/hamdashboard
'';
      };

      devShell = pkgs.mkShell {
        shellHook = ''
mkdir -p bin
rm -f bin/tsql
rm -f bin/cloudlogbashcat
rm -f bin/hamdashboard

ln -s ${packages.cloudlogbashcat}/bin/cloudlogbashcat bin/cloudlogbashcat
ln -s ${packages.hamdashboard}/bin/hamdashboard bin/hamdashboard

'';
        buildInputs = with pkgs; [
          # Process control
          python312Packages.supervisor

          # Chirp for programming the HT
          chirp

          # Hamlib CAT server
          hamlib_4

          # Direwolf Virtual TNC
          direwolf

          # Ardopc TNC
          ardopc

          # FLDigi for HF digital modes
          fldigi

          # Pat Winlink Client
          pat

          # CAT GUI
          flrig
          grig

          # logging software
          tqsl

          # js8call / ft8
          js8call
          wsjtx
          gridtracker

          # nanovna
          nanovna-saver

          # SSTV
          qsstv

          packages.FLEcli

          freedv

          potacatPkg

          packages.cloudlogbashcat

          # hardware flashing tools
          esptool
          esptool-ck
          dfu-util

          nmap
          inetutils

          # packages.hamdashboard

          # pulseaudio volume control (for qsstv)
          pavucontrol
          # pipewire volume control
          pwvucontrol
          alsa-utils

          chirp

          # echolink (qtel)
          svxlink

          # serial terminal
          tio
          screen

          # Paper QSL Tools
          glabels
          gum
          sane-airscan
          sane-frontends

          nixpkgs-radio.packages.${system}.hamclock

          openscad

          adif-multitool

          # tooling for the scripts in scripts/
          python312
          python312Packages.sh
          python312Packages.pydantic
          python312Packages.chevron
          python312Packages.docopt
          python312Packages.mypy
          python312Packages.pytest
          python312Packages.pytest-cov
        ];
      };
    }
  )) //
  {
    nixosModules.ft-991a = { lib, config, ...}:
      with lib;
      let
        cfg = config.hardware.ft-991a;
      in {
        options = {
          hardware.ft-991a = {
            enable = mkEnableOption "Yaesu FT-991a support";
          };
        };

        config = mkIf cfg.enable {
            services.udev.extraRules = ''
SUBSYSTEM=="tty", DRIVERS=="cp210x", ATTRS{interface}=="Standard*", SYMLINK+="ttyRigStandard"
SUBSYSTEM=="tty", DRIVERS=="cp210x", ATTRS{interface}=="Enhanced*", SYMLINK+="ttyRigEnhanced"
'';
        };
      };
  };
}
