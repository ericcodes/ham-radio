export BACKUP_HOME=~/Documents/digipi

mkdir -p "$BACKUP_HOME"

rsync -avH --delete pi@digipi:/home/pi/.local/ "$BACKUP_HOME/local"
rsync -avH --delete pi@digipi:/home/pi/.config/ "$BACKUP_HOME/config"
rsync -avH --delete pi@digipi:/home/pi/.fldigi/ "$BACKUP_HOME/fldigi"
rsync -avH --delete pi@digipi:/home/pi/.flrig/ "$BACKUP_HOME/flrig"
