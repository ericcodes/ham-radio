#!/usr/bin/env bash
set -eou pipefail
shopt -s nullglob

export SCANNER="escl:https://192.168.1.221:443"
export CARD_WIDTH_MM="139.7"
export CARD_HEIGHT_MM="88.9"

# Prompt
export DATE=$(gum input --placeholder "QSO Date (YYYY-MM-DD...)")
export CALLSIGN=$(gum input --placeholder "QSO Callsign")
# CARD_WIDTH=$(gum input --placeholder "Card Width" --value="$CARD_WIDTH")
# CARD_HEIGHT=$(gum input --placeholder "Card Width" --value="$CARD_HEIGHT")

export FRONT_FILENAME="logs/qsl-cards/$DATE-$CALLSIGN-front.png"
export BACK_FILENAME="logs/qsl-cards/$DATE-$CALLSIGN-back.png"

if [[ -e "$FRONT_FILENAME" || -e "$BACK_FILENAME" ]]; then
    if ! gum confirm "QSL images exist, overwrite?"; then
        exit 0
    fi
fi

# Wait for first side
until gum confirm "Is the front of the QSL Card in the scanner?"; do
    true
done

gum log -l info "Scanning front into $FRONT_FILENAME"
scanimage --device "$SCANNER" \
          --format=png \
          --mode color \
          --source "Flatbed" \
          -x "$CARD_WIDTH_MM" \
          -y "$CARD_HEIGHT_MM" \
          --resolution "300" \
          -o "$FRONT_FILENAME"

until gum confirm "Is the back of the QSL Card in the scanner?"; do
    true
done

gum log -l info "Scanning back into $BACK_FILENAME"
scanimage --device "$SCANNER" \
          --format=png \
          --mode color \
          --source "Flatbed" \
          -x "$CARD_WIDTH_MM" \
          -y "$CARD_HEIGHT_MM" \
          --resolution "300" \
          -o "$BACK_FILENAME"
