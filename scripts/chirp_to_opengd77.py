#!/usr/bin/env python3.12
import csv
from typing_extensions import Annotated
from typing import Optional, TextIO, Sequence
from pydantic import BaseModel, Field, PlainSerializer, BeforeValidator
from decimal import Decimal
import sys

def optional_decimal_serializer(val: Optional[Decimal]) -> str:
    if val is None:
        return 'None'
    return str(val)


def optional_decimal_validator(val: str) -> Optional[Decimal]:
    if val == 'None':
        return None
    return Decimal(val)


def empty_decimal_validator(val: str) -> Decimal:
    if val == '':
        val = 0
    return Decimal(val)


class ChirpRow(BaseModel):
    name: Annotated[str, Field(alias="Name")]
    location: Annotated[str, Field(alias="Location")]
    frequency: Annotated[Decimal, Field(alias="Frequency")]
    tone: Annotated[str, Field(alias="Tone")]
    r_tone_freq: Annotated[Decimal, Field(alias="rToneFreq")]
    duplex: Annotated[str, Field(alias="Duplex")]
    offset: Annotated[Decimal, Field(alias="Offset"), BeforeValidator(empty_decimal_validator)]


class OpenGD77Row(BaseModel):
    bandwidth: Annotated[str, Field(alias="Bandwidth (kHz)")] = "12.5"
    name: Annotated[str, Field(alias="Channel Name")]
    number: Annotated[str, Field(alias="Channel Number")]
    channel_type:  Annotated[str, Field(alias="Channel Type")] = "Analogue"
    rx_tone: Annotated[Optional[Decimal],
                       Field(alias="RX Tone"),
                       PlainSerializer(optional_decimal_serializer),
                       BeforeValidator(optional_decimal_validator)] = None
    rx_frequency: Annotated[Decimal,
                            Field(alias="Rx Frequency"),
                            PlainSerializer(str)]
    tx_tone: Annotated[Optional[Decimal],
                       Field(alias="TX Tone"),
                       PlainSerializer(optional_decimal_serializer),
                       BeforeValidator(optional_decimal_validator)] = None
    tx_frequency: Annotated[Decimal,
                            Field(alias="Tx Frequency"),
                            PlainSerializer(str)]



def chirp_to_opengd77(row: ChirpRow) -> OpenGD77Row:
    opengd77 = OpenGD77Row(**{
        "Channel Name": row.name,
        "Channel Number": row.location,
        "Rx Frequency": row.frequency,
        "Tx Frequency": row.frequency,
    })

    if row.duplex == "+":
         opengd77.tx_frequency = row.frequency + row.offset
    elif row.duplex == "-":
         opengd77.tx_frequency = row.frequency - row.offset

    if row.tone == "Tone":
        opengd77.tx_tone = row.r_tone_freq

    return opengd77


def write_rows(fh: TextIO, rows: Sequence[ChirpRow]):
    defaults = {
        'APRS': 'None',
        'All Skip': 'No',
        'Colour Code': '',
        'Contact': '',
        'DMR ID': '',
        'Latitude': '0',
        'Longitude': '0',
        'No Beep': 'No',
        'No Eco': 'No',
        'Rx Only': 'No',
        'Power': 'Master',
        'Squelch': 'Disabled',
        'TG List': '',
        'TOT': '0',
        'TS1_TA_Tx': '',
        'TS2_TA_Tx ID': '',
        'TX Tone': 'None',
        'Timeslot': '',
        'VOX': 'Off',
        'Zone Skip': 'No'
    }
    header = "Channel Number,Channel Name,Channel Type,Rx Frequency,Tx Frequency,Bandwidth (kHz),Colour Code,Timeslot,Contact,TG List,DMR ID,TS1_TA_Tx,TS2_TA_Tx ID,RX Tone,TX Tone,Squelch,Power,Rx Only,Zone Skip,All Skip,TOT,VOX,No Beep,No Eco,APRS,Latitude,Longitude".split(",")
    writer = csv.DictWriter(
        fh,
        fieldnames=header,
        delimiter=",",
        quotechar='"',
        lineterminator="\n",
    )

    writer.writeheader()
    for chirp_row in rows:
        data = dict(**defaults)
        opengd77_row = chirp_to_opengd77(chirp_row)
        data.update(opengd77_row.model_dump(by_alias=True))
        writer.writerow(data)


def convert(in_fh: TextIO, out_fh: TextIO):
    reader = csv.DictReader(in_fh, delimiter=",", quotechar='"')
    reader = (ChirpRow(**row) for row in reader)
    write_rows(out_fh, reader)

if __name__ == "__main__":
    convert(sys.stdin, sys.stdout)
