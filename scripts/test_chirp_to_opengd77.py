import unittest
from csv import DictReader
import csv
import io
import chirp_to_opengd77
from chirp_to_opengd77 import ChirpRow, OpenGD77Row
from decimal import Decimal

class TestChirpToOpenGD77Row(unittest.TestCase):
    def test_repeater_plus_txtone(self):
        chirp = ChirpRow.model_validate({
            'Comment': 'Cooksville',
            'Duplex': '+',
            'Frequency': '147.390000',
            'Location': '58',
            'Mode': 'FM',
            'Name': 'K3CUJ',
            'Offset': '0.600000',
            'Power': '50W',
            'RPT1CALL': '',
            'RPT2CALL': '',
            'RxDtcsCode': '023',
            'Skip': '',
            'TStep': '5.00',
            'Tone': 'Tone',
            'URCALL': '',
            'cToneFreq': '88.5',
            'rToneFreq': '156.7',
        })

        opengd77 = {
            'Bandwidth (kHz)': '12.5',
            'Channel Name': 'K3CUJ',
            'Channel Number': '58',
            'Channel Type': 'Analogue',
            'RX Tone': 'None',
            'Rx Frequency': '147.390000',
            'TX Tone': '156.7',
            'Tx Frequency': '147.990000',
        }

        self.assertEqual(
            opengd77,
            chirp_to_opengd77.chirp_to_opengd77(chirp).model_dump(by_alias=True))


    def test_repeater_minus_txtone(self):
        chirp = ChirpRow.model_validate({
            'Comment': 'Cooksville',
            'Duplex': '-',
            'Frequency': '147.990000',
            'Location': '58',
            'Mode': 'FM',
            'Name': 'K3CUJ',
            'Offset': '0.600000',
            'Power': '50W',
            'RPT1CALL': '',
            'RPT2CALL': '',
            'RxDtcsCode': '023',
            'Skip': '',
            'TStep': '5.00',
            'Tone': 'Tone',
            'URCALL': '',
            'cToneFreq': '88.5',
            'rToneFreq': '156.7',
        })

        opengd77 = {
            'Bandwidth (kHz)': '12.5',
            'Channel Name': 'K3CUJ',
            'Channel Number': '58',
            'Channel Type': 'Analogue',
            'RX Tone': 'None',
            'Rx Frequency': '147.990000',
            'TX Tone': '156.7',
            'Tx Frequency': '147.390000',
        }

        self.assertEqual(
            opengd77,
            chirp_to_opengd77.chirp_to_opengd77(chirp).model_dump(by_alias=True))

    def test_repeater_minus_notone(self):
        chirp = ChirpRow.model_validate({
            'Duplex': '-',
            'Frequency': '147.990000',
            'Location': '58',
            'Mode': 'FM',
            'Name': 'K3CUJ',
            'Offset': '0.600000',
            'Power': '50W',
            'RPT1CALL': '',
            'RPT2CALL': '',
            'RxDtcsCode': '023',
            'Skip': '',
            'TStep': '5.00',
            'Tone': '',
            'URCALL': '',
            'cToneFreq': '88.5',
            'rToneFreq': '88.5',
        })

        opengd77 = {
            'Bandwidth (kHz)': '12.5',
            'Channel Name': 'K3CUJ',
            'Channel Number': '58',
            'Channel Type': 'Analogue',
            'RX Tone': 'None',
            'Rx Frequency': '147.990000',
            'TX Tone': 'None',
            'Tx Frequency': '147.390000',
        }

        self.assertEqual(
            opengd77,
            chirp_to_opengd77.chirp_to_opengd77(chirp).model_dump(by_alias=True))

    def test_repeater_simplex(self):
        chirp = ChirpRow.model_validate({
            'Duplex': '',
            'Frequency': '146.520000',
            'Location': '58',
            'Mode': 'FM',
            'Name': '2m call',
            'Offset': '',
            'Power': '50W',
            'RPT1CALL': '',
            'RPT2CALL': '',
            'RxDtcsCode': '023',
            'Skip': '',
            'TStep': '5.00',
            'Tone': '',
            'URCALL': '',
            'cToneFreq': '88.5',
            'rToneFreq': '88.5',
        })

        opengd77 = {
            'Bandwidth (kHz)': '12.5',
            'Channel Name': '2m call',
            'Channel Number': '58',
            'Channel Type': 'Analogue',
            'RX Tone': 'None',
            'Rx Frequency': '146.520000',
            'TX Tone': 'None',
            'Tx Frequency': '146.520000',
        }

        self.assertEqual(
            opengd77,
            chirp_to_opengd77.chirp_to_opengd77(chirp).model_dump(by_alias=True))


class TestChirpToOpenGD77Files(unittest.TestCase):
    maxDiff = None
    def test_convert(self):
        out_fh = io.StringIO()
        with open("test/fixtures/chirp.csv") as in_fh:
            chirp_to_opengd77.convert(in_fh, out_fh)

        self.assertEqual(
            """Channel Number,Channel Name,Channel Type,Rx Frequency,Tx Frequency,Bandwidth (kHz),Colour Code,Timeslot,Contact,TG List,DMR ID,TS1_TA_Tx,TS2_TA_Tx ID,RX Tone,TX Tone,Squelch,Power,Rx Only,Zone Skip,All Skip,TOT,VOX,No Beep,No Eco,APRS,Latitude,Longitude
0,K3CUJ,Analogue,146.390000,146.990000,12.5,,,,,,,,None,156.7,Disabled,Master,No,No,No,0,Off,No,No,None,0,0
1,RPT2,Analogue,147.000000,146.400000,12.5,,,,,,,,None,None,Disabled,Master,No,No,No,0,Off,No,No,None,0,0
2,2m call,Analogue,146.520000,146.520000,12.5,,,,,,,,None,None,Disabled,Master,No,No,No,0,Off,No,No,None,0,0
""",
            out_fh.getvalue(),
        )
